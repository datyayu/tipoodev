import json
from webapp2 import RedirectHandler
from google.appengine.ext import db

from Model.Subject.Topic import Topic
from Model.ContentMaterial.VideoMaterial import VideoMaterial
from Model.ContentMaterial.TextMaterial import TextMaterial


class TopicsAPI(RedirectHandler):
    ''' Concept index '''
    def get(self):
        # Set query params.
        limit = int(self.request.get('limit').encode('utf-8')) if self.request.get('limit') else 99
        query = self.request.get('q').encode('utf-8').lower() if self.request.get('q') else None

        # Find concepts in db.
        topics = Topic.all().fetch(limit=limit)

        # Create a response with the concept list.
        data = []
        for topic in topics:
            videos = VideoMaterial.all() \
                                  .filter('available =', True) \
                                  .filter('topic =', topic.key()) \
                                  .fetch(limit=limit)
            texts = TextMaterial.all() \
                                .filter('available =', True) \
                                .filter('topic =', topic.key()) \
                                .fetch(limit=limit)

            data.append({
                'name': topic.name,
                'definition': topic.description,
                'videos': [int(video.key().id()) for video in videos],
                'texts': [int(text.key().id()) for text in texts],
            })

        # Send the concept list to user.
        response_data = json.dumps(data)

        self.response.status = 200
        self.response.headers['Content-Type'] = "application/json"
        self.response.write(response_data)
