import json
from os import getenv
from time import time
from datetime import datetime, timedelta
from Vendor.jwt import encode

from Controller.Handler.RequestManager import RequestManagement
from Controller.Management.UserManagement import StudentManagement

JWT_KEY = getenv('JWT_KEY')


class JWTRequest(RequestManagement):
    def get(self):
        user_type = self.get_user_type()
        student_manager = StudentManagement()

        if user_type != 'Student':
            self.response.status = 400
            self.response.headers['Content-Type'] = "application/json"
            return self.response.write({'error': 'You are not a student.'})

        student_id = self.get_user_id()
        student_entity = student_manager.get_student(student_id)
        student = student_entity.__dict__['_entity']

        print int(self.session['user-time'])
        token_data = {
            'id': self.session['user-id'],
            'name': student['first'],
            'email': student['email'],
            'session-time': int(self.session['user-time']),
            'iat': int(time()),
            'exp': datetime.now() + timedelta(minutes=1)
        }

        generated_token = encode(token_data, JWT_KEY, algorithm='HS256')
        json_response = json.dumps({'token': generated_token})

        self.response.status = 200
        self.response.headers['Content-Type'] = "application/json"
        return self.response.write(json_response)
